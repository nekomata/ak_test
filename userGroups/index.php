<?php

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');

$APPLICATION->IncludeComponent(
    'ak:user.groups',
    '.default',
    array(
        'COMPONENT_TEMPLATE' => '.default',
        'SEF_MODE' => 'Y',
        'SEF_FOLDER' => '/userGroups/',
        'GROUP_LIST_TITLE' => 'Список пользовательских групп',
        'CACHE_TYPE' => 'A',
        'CACHE_TIME' => '60',
        'SEF_URL_TEMPLATES' => array(
            'group.list' => '',
            'group' => '#GROUP_ID#/',
        )
    ),
    false
);

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/footer.php');
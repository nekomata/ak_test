<?php

$MESS ['GROUP_LIST_TITLE'] = 'Заголовок страницы списка пользовательских групп';
$MESS ['GROUP_LIST_TITLE_DEFAULT'] = 'Список пользовательских групп';
$MESS ['USER_GROUP_LIST_PAGE'] = 'Страница списка групп';
$MESS ['USER_GROUP_PAGE'] = 'Страница группы';
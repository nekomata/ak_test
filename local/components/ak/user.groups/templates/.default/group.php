<?php

/** @var array $arParams */

/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$APPLICATION->IncludeComponent(
    'ak:user.group.detail',
    $templateName,
    [
        'GROUP_ID' => $arResult['VARIABLES']['GROUP_ID'],
        'CACHE_TIME' => $arParams['CACHE_TIME'],
    ],
    $component
);
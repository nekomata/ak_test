<?php

/** @var array $arParams */

/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */

/** @var CBitrixComponent $component */

use Bitrix\Main\GroupTable;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

$userGroups = GroupTable::getList(
    [
        'select' => ['NAME', 'ID', 'DESCRIPTION'],
        'filter' => ['ACTIVE' => 'Y'],
        'cache' => [
            'ttl' => $arParams['CACHE_TIME']
        ]
    ]
)->fetchAll();
?>
    <h1><?= $arParams['GROUP_LIST_TITLE'] ?></h1>
<?php
foreach ($userGroups as $userGroup) {
    $href = str_replace('#GROUP_ID#',$userGroup['ID'],($arResult['FOLDER'].$arResult['URL_TEMPLATES']['group']));
    ?>
    <div class='groupBlock'>
        <a href="<?=$href?>" target="_blank">
            <p class='groupTitle'><?= GetMessage('GROUP_TITLE') . ' ' . $userGroup['NAME'] ?>(<?= $userGroup['ID'] ?>)</p>
        </a>
        <p class='groupDescription'><?= $userGroup['DESCRIPTION']?:GetMessage('EMPTY_DESCRIPTION') ?></p>
    </div>
    <?php
}
<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/** @var array $arCurrentValues */

$arComponentParameters = [
    'GROUPS' => [],
    'PARAMETERS' => [
        'GROUP_LIST_TITLE' => [
            'PARENT' => 'BASE',
            'NAME' => GetMessage('GROUP_LIST_TITLE'),
            'TYPE' => 'STRING',
            'DEFAULT' => GetMessage('GROUP_LIST_TITLE_DEFAULT'),
        ],
        'CACHE_TIME' => ['DEFAULT' => 3600],
        'SEF_MODE' => [
            'group.list' => [
                'NAME' => GetMessage('USER_GROUP_LIST_PAGE'),
                'DEFAULT' => '',
                'VARIABLES' => [
                ],
            ],
            'group' => [
                'NAME' => GetMessage('USER_GROUP_PAGE'),
                'DEFAULT' => '#GROUP_ID#/',
                'VARIABLES' => [
                    'GROUP_ID',
                ],
            ],
        ],
    ]
];
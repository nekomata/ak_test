<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$APPLICATION->SetTitle(GetMessage('TITLE'));

?>

<h1><?= GetMessage('TITLE') ?></h1>

<div class='groupBlock'>
    <p class='groupTitle'><?= GetMessage('GROUP_TITLE') . ' ' . $arResult['GROUP']['NAME'] ?>(<?= $arResult['GROUP']['ID'] ?>)</p>
    <p class='groupDescription'><?= $arResult['GROUP']['DESCRIPTION'] ?></p>
</div>
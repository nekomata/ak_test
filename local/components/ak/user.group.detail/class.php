<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\GroupTable;

/**
 * Class AKUserGroupDetailComponent
 */
class AKUserGroupDetailComponent extends CBitrixComponent
{

    /**
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        return $arParams;
    }

    /**
     * @return mixed|void|null
     */
    public function executeComponent()
    {
        $this->arResult['GROUP'] = $this->getGroupById($this->arParams['GROUP_ID']);

        $this->includeComponentTemplate();
    }

    /**
     * @param int $groupId
     * @return array
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    private function getGroupById(int $groupId): array
    {
        return GroupTable::getList(
            [
                'select' => ['NAME', 'ID', 'DESCRIPTION'],
                'filter' => ['ACTIVE' => 'Y', 'ID' => $groupId],
                'cache' => [
                    'ttl' => $this->arParams['CACHE_TIME']
                ]
            ]
        )->fetch();
    }

}